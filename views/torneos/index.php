<?php
use app\models\Reservas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->title = 'Torneos';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>


    <?php if (!Yii::$app->user->isGuest): ?>
    <p>
        <?= Html::a('Crear Torneo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php endif; ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'codigo_t',
            'nombre',
            'organizadores',
            'fecha',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_t' => $model->codigo_t]);
                 }
            ],
        ],
    ]); ?>
